<?php

use yii\web\View;
use common\widgets\GridView;
use ffsoft\export\helpers\HtmlRender;

/**
 * @var $this       View
 * @var $content    string|Closure
 */
?>

<?= $content; ?>
</div></div>
</body></html>