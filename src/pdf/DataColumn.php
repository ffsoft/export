<?php

namespace ffsoft\export\pdf;

use yii\helpers\ArrayHelper;

class DataColumn extends \yii\grid\DataColumn
{
    public $precision = null;

    /**
     * Returns the data cell value.
     *
     * @param mixed $model the data model
     * @param mixed $key   the key associated with the data model
     * @param int   $index the zero-based index of the data model among the models array returned by
     *                     [[GridView::dataProvider]].
     *
     * @return string the data cell value
     */
    public function getDataCellValue($model, $key, $index)
    {
        $value = null;

        if ($this->value !== null) {
            if (is_string($this->value)) {
                $value = ArrayHelper::getValue($model, $this->value);
            }

            $value = call_user_func($this->value, $model, $key, $index, $this);
        } elseif ($this->attribute !== null) {
            $value = ArrayHelper::getValue($model, $this->attribute);
        }

        if ($value !== null) {
            if ($this->format === 'decimal') {

            }
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $content = null;

        if ($this->format !== 'decimal') {
            $content = parent::renderDataCellContent($model, $key, $index);
        } else {
            if (isset($this->precision) && $this->precision instanceof \Closure) {
                $precision = call_user_func($this->precision, $model, $key, $index, $this);

                if ($precision) {
                    $content = number_format($this->getDataCellValue($model, $key, $index), $precision, '.', ' ');
                }
            }
        }

        return $content;
    }
}