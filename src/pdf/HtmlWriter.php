<?php

namespace ffsoft\export\pdf;

use Closure;
use common\helpers\DHelper;
use Exception;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
use yii\db\BatchQueryResult;
use yii\grid\Column;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\i18n\Formatter;

class HtmlWriter
{
    /**
     * @var string the default data column class if the class name is not explicitly specified when configuring a data
     *      column. Defaults to 'yii\grid\DataColumn'.
     */
    public $dataColumnClass;
    /**
     * @var array the HTML attributes for the table header row.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $headerRowOptions = [];
    /**
     * @var array|Closure the HTML attributes for the table body rows. This can be either an array
     * specifying the common HTML attributes for all body rows, or an anonymous function that
     * returns an array of the HTML attributes. The anonymous function will be called once for every
     * data model returned by [[dataProvider]]. It should have the following signature:
     *
     * ```php
     * function ($model, $key, $index, $grid)
     * ```
     *
     * - `$model`: the current data model being rendered
     * - `$key`: the key value associated with the current data model
     * - `$index`: the zero-based index of the data model in the model array returned by [[dataProvider]]
     * - `$grid`: the GridView object
     *
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $rowOptions = [];
    /**
     * @var \yii\data\DataProviderInterface the data provider for the view. This property is required.
     */
    public $dataProvider;
    /**
     * @var string the layout that determines how different sections of the list view should be organized.
     * The following tokens will be replaced with the corresponding section contents:
     */
    public $layout = "{items}";
    /**
     * @var array grid column configuration. Each array element represents the configuration
     * for one particular grid column. For example,
     */
    /**
     * @var bool whether to show the grid view if [[dataProvider]] returns no data.
     */
    public $showOnEmpty = true;
    /**
     * @var bool whether to show the header section of the grid table.
     */
    public $showHeader = true;
    /**
     * @var bool whether to show the footer section of the grid table.
     */
    public $showFooter = false;
    /**
     * @var array|Formatter the formatter used to format model attribute values into displayable texts.
     * This can be either an instance of [[Formatter]] or an configuration array for creating the [[Formatter]]
     * instance. If this property is not set, the "formatter" application component will be used.
     */
    public $formatter;
    /**
     * @var array the HTML attributes for the container tag of the grid view.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = ['class' => 'grid-view'];
    public $columns = [];
    public $batchSize = 100;
    public $tmpFilePath = '';
    public $title = '';
    public $beforeContent = '';
    public $afterContent = '';

    /**
     * Runs the widget.
     */
    public function run()
    {
        if ($this->dataProvider === null) {
            throw new InvalidConfigException('The "dataProvider" property must be set.');
        }

        if ($this->formatter === null) {
            $this->formatter = Yii::$app->getFormatter();
        } elseif (is_array($this->formatter)) {
            $this->formatter = Yii::createObject($this->formatter);
        }

        if (!$this->formatter instanceof Formatter) {
            throw new InvalidConfigException('The "formatter" property must be either a Format object or a configuration array.');
        }

        $this->initColumns();

        if ($this->showOnEmpty || $this->dataProvider->getCount() > 0) {
            $content = $this->renderItems();
        } else {
            $content = $this->renderEmpty();
        }
    }

    /**
     * Creates a [[DataColumn]] object based on a string in the format of "attribute:format:label".
     * @param string $text the column specification string
     * @return DataColumn the column instance
     * @throws InvalidConfigException if the column specification is invalid
     */
    protected function createDataColumn($text)
    {
        if (!preg_match('/^([^:]+)(:(\w*))?(:(.*))?$/', $text, $matches)) {
            throw new InvalidConfigException('The column must be specified in the format of "attribute", "attribute:format" or "attribute:format:label"');
        }

        return Yii::createObject([
            'class' => $this->dataColumnClass ?: DataColumn::class,
            'grid' => $this,
            'attribute' => $matches[1],
            'format' => isset($matches[3]) ? $matches[3] : 'text',
            'label' => isset($matches[5]) ? $matches[5] : null,
        ]);
    }

    /**
     * Creates column objects and initializes them.
     */
    protected function initColumns()
    {
        if (empty($this->columns)) {
            $this->guessColumns();
        }
        foreach ($this->columns as $i => $column) {
            if (is_string($column)) {
                $column = $this->createDataColumn($column);
            } else {
                $column = Yii::createObject(array_merge([
                    'class' => $this->dataColumnClass ?: DataColumn::class,
                    'grid'  => $this,
                ], $column));
            }
            if (!$column->visible) {
                unset($this->columns[$i]);
                continue;
            }
            $this->columns[$i] = $column;
        }
    }

    public function renderCaption()
    {
        if (!empty($this->caption)) {
            $line = Html::tag('caption', $this->caption, $this->captionOptions);
            file_put_contents($this->tmpFilePath, $line, FILE_APPEND);
        }

        return false;
    }

    public function renderColumnGroup()
    {
        file_put_contents($this->tmpFilePath, '<colgroup>', FILE_APPEND);

        foreach ($this->columns as $column) {
            /* @var $column Column */
            if (!empty($column->options)) {
                $cols = [];
                foreach ($this->columns as $col) {
                    $line = Html::tag('col', '', $col->options);
                    file_put_contents($this->tmpFilePath, $line, FILE_APPEND);
                }
            }
        }
        file_put_contents($this->tmpFilePath, '</colgroup>', FILE_APPEND);

        return false;
    }

    /**
     * Renders the table header.
     *
     * @return string the rendering result.
     */
    public function renderTableHeader()
    {
        $cells = [];
        foreach ($this->columns as $column) {
            /* @var $column Column */
            $cells[] = $column->renderHeaderCell();
        }

        $content = Html::tag('tr', implode('', $cells), $this->headerRowOptions);

        file_put_contents($this->tmpFilePath, "<thead>\n" . $content . "\n</thead>", FILE_APPEND);
    }

    /**
     * Renders the table footer.
     *
     * @return string the rendering result.
     */
    public function renderTableFooter()
    {
        file_put_contents($this->tmpFilePath, '</tfoot>', FILE_APPEND);

        $cells = [];
        foreach ($this->columns as $column) {
            /* @var $column Column */
            $cells[] = $column->renderFooterCell();
        }

        $content = Html::tag('tr', implode('', $cells), $this->footerRowOptions);

        file_put_contents($this->tmpFilePath, $content, FILE_APPEND);
        file_put_contents($this->tmpFilePath, '</tfoot>', FILE_APPEND);
    }

    public function renderHeader()
    {
        return Yii::$app->controller->renderPartial('@ffsoft/export/views/pdf/header.php', [
            'title'   => $this->title,
            'content' => $this->beforeContent,
        ]);
    }

    public function renderFooter()
    {
        return Yii::$app->controller->renderPartial('@ffsoft/export/views/pdf/footer.php', [
            'title'   => $this->title,
            'content' => $this->afterContent,
        ]);
    }

    /**
     * Renders the data models for the grid view.
     *
     * @return string the HTML code of table
     */
    public function renderItems()
    {
        file_put_contents(
            $this->tmpFilePath,
            $this->renderHeader() . '<table class="table table-bordered">',
            FILE_APPEND
        );

        $this->renderCaption();
        $this->renderColumnGroup();
        $this->showHeader ? $this->renderTableHeader() : false;
        $this->renderTableBody();

        $tableFooter = false;
        $tableFooterAfterBody = false;

        if ($this->showFooter) {
            if ($this->placeFooterAfterBody) {
                $tableFooterAfterBody = $this->renderTableFooter();
            } else {
                $tableFooter = $this->renderTableFooter();
            }
        }

        file_put_contents($this->tmpFilePath, '</table>' . $this->renderFooter(), FILE_APPEND);
    }

    /**
     * Renders the HTML content indicating that the list view has no data.
     *
     * @return string the rendering result
     * @see emptyText
     */
    public function renderEmpty()
    {
        if ($this->emptyText === false) {
            return '';
        }
        $options = $this->emptyTextOptions;
        $tag = ArrayHelper::remove($options, 'tag', 'div');

        return Html::tag($tag, $this->emptyText, $options);
    }

    /**
     * @return BatchQueryResult the row records in batches of `$batchSize`
     * @throws Exception
     */
    public function getData()
    {
        return $this->getQuery()->each($this->batchSize);
    }

    /**
     * @return yii\db\ActiveQuery the query for the data
     * @throws Exception
     */
    public function getQuery()
    {
        if ($this->dataProvider === null || !($this->dataProvider instanceof ActiveDataProvider)) {
            throw new Exception('No query set');
        }

        return $this->dataProvider->query;
    }

    /**
     * Renders a table row with the given data model and key.
     *
     * @param mixed $model the data model to be rendered
     * @param mixed $key   the key associated with the data model
     * @param int   $index the zero-based index of the data model among the model array returned by [[dataProvider]].
     *
     * @return string the rendering result
     */
    public function renderTableRow($model, $key, $index)
    {
        $cells = [];
        /* @var $column Column */
        foreach ($this->columns as $column) {
//            $column->contentOptions = ['style' => 'padding: 10px; border: 1px solid #ddd;'];
            $cells[] = $column->renderDataCell($model, $key, $index);
        }

        if ($this->rowOptions instanceof Closure) {
            $options = call_user_func($this->rowOptions, $model, $key, $index, $this);
        } else {
            $options = $this->rowOptions;
        }

        $options['data-key'] = is_array($key) ? json_encode($key) : (string)$key;

        $line = Html::tag('tr', implode('', $cells), $options);
        file_put_contents($this->tmpFilePath, $line, FILE_APPEND);
    }

    /**
     * Renders the table body.
     *
     * @return string the rendering result.
     */
    public function renderTableBody()
    {
        $rows = [];
        file_put_contents($this->tmpFilePath, '<tbody>', FILE_APPEND);

        foreach ($this->getData() as $index => $model) {
            $key = !empty($model->id) ? $model->id : $index;
            $this->renderTableRow($model, $key, $index);
        }

        file_put_contents($this->tmpFilePath, '</tbody>', FILE_APPEND);
    }
}