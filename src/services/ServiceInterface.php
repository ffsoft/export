<?php

namespace ffsoft\export\services;

interface ServiceInterface
{
    public function run();
}