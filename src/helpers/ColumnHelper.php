<?php

namespace ffsoft\export\helpers;

class ColumnHelper
{
    public static function setColumnsId($columns)
    {
        $result = [];

        foreach ($columns as $index => $column) {
            $result[$index] = $column;
            $result[$index]['columnId'] = self::$columnIds[$index];
        }

        return $result;
    }

    protected static $columnIds
        = [
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z',
            'AA',
            'AB',
            'AC',
            'AD',
            'AE',
            'AF',
            'AG',
            'AH',
            'AI',
            'AJ',
            'AK',
            'AL',
            'AM',
            'AN',
            'AO',
            'AP',
            'AQ',
            'AR',
            'AS',
            'AT',
            'AU',
            'AV',
            'AW',
            'AX',
            'AY',
            'AZ',
        ];
}