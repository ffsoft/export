<?php

namespace ffsoft\export\excel;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\BaseWriter;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;

class ExcelFile extends BaseObject
{
    /**
     * @var array options to pass to the constructor of \mikehaertl\tmp\File,
     * indexed by option name. Available keys are 'suffix', 'prefix' and
     * 'directory'.  This is only useful if creation of the temporary file
     * fails for some reason.
     */
    public $fileOptions = [];
    /**
     * @var string the writer class to use. Default is
     * `BaseWriter`.
     */
    public $writerClass = Xlsx::class;
    /** TODO: Убрать _ */
    protected $_fileCreated = false;
    protected $_sheets;
    protected $_sheetsCreated = false;
    protected $_tmpFile;
    protected $_workbook;
    protected $_writer;

    /**
     * Create the Excel sheets if they were not created yet
     *
     * @throws InvalidConfigException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \Exception
     */
    public function createSheets()
    {
        if (!$this->_sheetsCreated) {
            $workbook = $this->getWorkbook();
            $i = 0;
            foreach ($this->_sheets as $title => $config) {
                if (is_string($config)) {
                    $config = ['class' => $config];
                } elseif (is_array($config)) {
                    if (!isset($config['class'])) {
                        $config['class'] = ExcelSheet::class;
                    }
                } elseif (!is_object($config)) {
                    throw new \Exception('Invalid sheet configuration');
                }

                $activeSheet = ($i === 0) ? $workbook->getActiveSheet() : $workbook->createSheet();
                if (is_string($title)) {
                    $activeSheet->setTitle($title);
                }
                /** @var ActiveExcelSheet $sheet */
                $sheet = Yii::createObject($config, [$activeSheet]);
                $sheet->render();
                $i++;
            }
            $this->_sheetsCreated = true;
        }
    }

    /**
     * @return array the sheet configuration
     */
    public function getSheets()
    {
        return $this->_sheets;
    }

    public function getWorkbook()
    {
        if ($this->_workbook === null) {
            $this->_workbook = new Spreadsheet();
        }

        return $this->_workbook;
    }

    /**
     * @return BaseWriter the writer instance
     */
    public function getWriter()
    {
        if ($this->_writer === null) {
            $class = $this->writerClass;
            $this->_writer = new $class($this->getWorkbook());
        }

        return $this->_writer;
    }

    /**
     * @param string|null $filename    the filename to send. If empty, the file is streamed inline.
     * @param string      $contentType the Content-Type header. Default is 'application/vnd.ms-excel'.
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function send($filename = null, $contentType = 'application/vnd.ms-excel')
    {
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Type: ' . $contentType);
        header('Content-Transfer-Encoding: binary');

        if ($filename !== null) {
            header("Content-Disposition: inline; filename=\"$filename\"");
        }

        ob_end_clean();

        $this->createFile();
        exit;
    }

    /**
     * @param array $value the sheet configuration. This must be an array where
     *                     keys are sheet names and values are arrays with the configuration
     *                     options for an instance if `ExcelSheet`.
     */
    public function setSheets($value)
    {
        $this->_sheets = $value;
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     * @throws InvalidConfigException
     */
    protected function createFile()
    {
        if (!$this->_fileCreated) {
            $this->createSheets();
            $this->getWriter()->save('php://output');
            $this->_fileCreated = true;
        }
    }
}