<div class="btn-group">

    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="true"><?= Yii::t('common', 'Export'); ?>
    </button>

    <div class="dropdown-menu" x-placement="bottom-start">
        <button class="dropdown-item" id="excel-export-button" name="exportType" value="excel" type="submit">
                <span>
                    <i class="m-nav__link-icon la la-file-excel-o"></i>
                    <span><?= Yii::t('common', 'Excel'); ?></span>
                </span>
        </button>
        <button class="dropdown-item" id="pdf-export-button" name="exportType" value="pdf" type="submit">
                <span>
                    <i class="m-nav__link-icon la la-file-pdf-o"></i>
                    <span><?= Yii::t('common', 'PDF'); ?></span>
                </span>
        </button>
    </div>

</div>