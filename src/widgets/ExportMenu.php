<?php

namespace ffsoft\export\widgets;

use yii\bootstrap\Widget;

class ExportMenu extends Widget
{
    public $title;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('buttons');
    }
}